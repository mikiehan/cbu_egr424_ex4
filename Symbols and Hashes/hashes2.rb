# define hash family_tree_19 with symbol :oldest :older :younger
# using symbol: syntax
#family_tree_19 = 

# reterive the value of the entry with key :youngest 
family_tree_19[:youngest] = "Jeremy" 

# inspect family_tree_19
#p family_tree_19 

# Named parameter "like" behavior... 
# define adjust_colors method with some default parameter that takes in a hash as the argument
#def adjust_colors (props = ) 
#  puts  
#  puts  
#end

# call adjust_colors without any argument
#adjust_colors 

# call adjust_colors with different hash with different syntax
#adjust_colors ({ }) 
#adjust_colors  
#adjust_colors 
