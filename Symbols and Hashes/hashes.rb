# fill in the following hash called editor_props
#editor_props = 

# THE ABOVE IS NOT A BLOCK - IT'S A HASH 
# get hash length/size
#puts editor_props.length # => 3

# retrieve a value with key "font"
#puts 

# update entry with key "background" so that its new value is "Blue"
#editor_props["background"] = "Blue" 


# Ouput key: xxx value: xxx for each pair in editor_props using multi-line block
# => Key: font value: Arial 
# => Key: size value: 12 
# => Key: color value: red 
# => Key: background value: Blue
#
#
#

