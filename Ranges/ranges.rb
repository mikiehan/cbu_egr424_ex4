# Assign range from 1-3 to some_range (use two dots)
#some_range = 

# Output the max of some_range
#puts

# Output whether some_range includes 2
#puts 

# Output whether a range from 1~10 includes 5.3 using triple equal
#puts 

# Output whether 5.3 includes a range from 1~10 using triple equal 
#puts

# Ranges with three dots are end-exclusive
#puts

# Convert ranges from 'k' to 'z' to array and randomly pick any two elements
#p 

# Fill in the following case statement
#age = 55 
#case age 
#  when  
#  when  
#  else  
#end 
