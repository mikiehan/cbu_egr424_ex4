# define class Person
# Define an instance variable called age with attr_reader
# Define another instance variable called name with attr_accessor

#class Person 
#
#

  # Constructor 
#  def initialize (name, ageVar)  
#
#  end 
  
  # Define our own setter method for age
#  def age= (new_age) 
#
#  end 

#end 

# After implementing above, uncomment the following and run
#person1 = Person.new("Kim", 13) # => 13 
#puts "My age is #{person1.age}" # => My age is 13 
#person1.age = 130 # Try to change the age 
#puts person1.age # => 13 (The setter didn't allow the change)