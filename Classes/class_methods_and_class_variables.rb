# define class methods
class MathFunctions 
  #define class method double using self. (First way)
  
  
  #define class method times_called using << self (Second way)
  
  
  
  def hello_world
    "hello_world"
  end
end 

# define class method called triple under MathFunctions class (Third way)


# No instance created! 
#puts MathFunctions.double 5 # => 10 
#puts MathFunctions.triple(3) # => 9 
#puts MathFunctions.times_called # => 3 

# If you need to call hello_world, you need to instantiate an object and call the method on the object
#o1 = MathFunctions.new
#puts o1.hello_world
