# define class Person 
# with attr_accessor for two instance variables name age
class Person 
  
end 

# Create a new instance person1
#person1 = Person.new 

# inspect person1's name
#p person1.name 

# set person's name to Mike and age to 15
#person1.name  
#person1.age 

# inspect person1
#p person1

# set person1's age to string value
#person1.age 

# output person1's age
#puts person1.age
